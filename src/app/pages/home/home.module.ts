import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HeroComponent } from './components/hero/hero.component';
import { AboutPsychoterapyComponent } from './components/about-psychoterapy/about-psychoterapy.component';
import { CiteComponent } from './components/cite/cite.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeRoutingModule } from './home-routing.module';


@NgModule({
  declarations: [HomeComponent, HeroComponent, AboutPsychoterapyComponent, CiteComponent, ContactComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule {
}
