import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutPsychoterapyComponent } from './about-psychoterapy.component';

describe('AboutPsychoterapyComponent', () => {
  let component: AboutPsychoterapyComponent;
  let fixture: ComponentFixture<AboutPsychoterapyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutPsychoterapyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutPsychoterapyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
